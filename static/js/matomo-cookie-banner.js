// Creare's 'Implied Consent' EU Cookie Law Banner v:2.4
// Conceived by Robert Kent, James Bavington & Tom Foyster
// Put into a namespace and by Bjørn Rosell
//   Also changed behaviour so you have to click accept to make the banner stay away.
//   To make it behave like the original, set "createCookieWhenBannerIsShown" to true.

var CookieBanner = (function() {
    return {
        'createCookieWhenBannerIsShown': false,
        'createCookieWhenAcceptIsClicked': true,
        'cookieDuration': 14,                   // Number of days before the cookie expires, and the banner reappears
        'cookieName': 'cookieConsent',          // Name of our cookie
        'cookieValue': 'accepted',              // Value of cookie

        '_createDiv': function(html) {
            var bodytag = document.getElementsByTagName('body')[0];
            var div = document.createElement('div');
            div.setAttribute('id','cookie-law');
            div.innerHTML = html;

            // bodytag.appendChild(div); // Adds the Cookie Law Banner just before the closing </body> tag
            // or
            bodytag.insertBefore(div,bodytag.firstChild); // Adds the Cookie Law Banner just after the opening <body> tag

            document.getElementsByTagName('body')[0].className+=' cookiebanner'; //Adds a class tothe <body> tag when the banner is visible

            if (CookieBanner.createCookieWhenBannerIsShown) {
                CookieBanner.createAcceptCookie();
            }
        },

        '_checkCookie': function(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
        },

        'closeBanner': function() {
            var element = document.getElementById('cookie-law');
            element.parentNode.removeChild(element);
        },

        'accept': function() {
            _paq.push(['rememberConsentGiven', 336]);
            CookieBanner.closeBanner();
        },

        'decline': function() {
            CookieBanner.closeBanner();
        },

        'consent': function () {
          return CookieBanner._checkCookie('mtm_consent') != null
        },

        'showUnlessAccepted': function(html) {
            //alert(CookieBanner._checkCookie(CookieBanner.cookieName));
            //alert(document.cookie);
            if(!CookieBanner.consent()){
                CookieBanner._createDiv(html);
            }
        }

    }

})();

window.onload = function(){
    //var html = '<p>Our website uses cookies. By continuing we assume your permission to deploy cookies, as detailed in our <a href="/privacy-cookies-policy/" rel="nofollow" title="Privacy &amp; Cookies Policy">privacy and cookies policy</a>. <a class="close-cookie-banner" href="javascript:void(0);" onclick="CookieBanner.accept();"><span>X</span></a></p>'
    var html = '<div>' +
        'This site uses the free software <a href="https://matomo.org">Matomo</a> to analyze traffic and help us to improve your user experience.<br>We process your email address and IP address and cookies are stored on your browser for 13 months. This data is only processed by us and our web hosting platform.<br>' +
        'For further information, please review our ' +
        '<a href="/legal/imprint/">imprint</a>. ' +
        'By clicking "Accept", you agree to our use of Matomo.' +
        '</div>'

    // Add the accept button
    html += '<div><a href="javascript:void(0);" onclick="CookieBanner.accept();"><span>Accept</span></a></div>';
    html += '<div><a href="javascript:void(0);" onclick="CookieBanner.decline();"><span>Decline</span></a></div>';
    CookieBanner.showUnlessAccepted(html);
}
